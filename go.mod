module github.com/mangostine-benchmark-go

go 1.15

require (
	github.com/ethereum/go-ethereum v1.9.23
	github.com/fxamacker/cbor v1.5.1 // indirect
	github.com/fxamacker/cbor/v2 v2.2.0 // indirect
	github.com/golang/protobuf v1.4.3
	github.com/spf13/cobra v1.1.1
	github.com/tyler-smith/go-bip39 v1.1.0
	github.com/ugorji/go v1.1.13 // indirect
	github.com/ugorji/go/codec v1.1.13 // indirect
	github.com/zeebo/blake3 v0.1.0
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897
	golang.org/x/net v0.0.0-20201031054903-ff519b6c9102 // indirect
	golang.org/x/sys v0.0.0-20201101102859-da207088b7d1 // indirect
	golang.org/x/text v0.3.4 // indirect
	google.golang.org/genproto v0.0.0-20201104152603-2e45c02ce95c // indirect
	google.golang.org/grpc v1.33.1
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.0.1 // indirect
	google.golang.org/protobuf v1.25.0
	lukechampine.com/blake3 v1.1.4 // indirect
)
