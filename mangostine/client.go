package mangostine

import (
	"context"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	pb "github.com/mangostine-benchmark-go/grpc/mangostine_v0"
	"google.golang.org/grpc"
)

const (
	address     = "localhost:16657"
	defaultName = "world"
)

//create the instance of grpc client
func SendRawTxClient(tx []byte) {

	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewRpcClient(conn)

	// Contact the server and print out its response.
	name := defaultName
	log.Printf("transaction name %v", name)
	if len(os.Args) > 1 {
		name = os.Args[1]
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	r, err := c.SendRawTx(ctx, &pb.RawTransaction{RawTx: tx})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	log.Printf("transaction completed %v", r)
}

//create the instance of grpc client
func SendTXClient(tx *Transaction) {

	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewRpcClient(conn)

	// Contact the server and print out its response.
	name := defaultName
	log.Printf("transaction name %v", name)
	if len(os.Args) > 1 {
		name = os.Args[1]
	}
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()
	var sends []*pb.Send
	send := &pb.Send{
		To:     hex.EncodeToString(tx.Payload.Recipients.Address),
		Amount: tx.Payload.Recipients.Amount,
	}
	sends = append(sends, send)
	address := hex.EncodeToString(tx.Payload.Sender)
	r, err := c.SendTx(ctx, &pb.Transaction{Sender: address, Receiver: sends, PublicKey: tx.Public_key, Sequence: tx.Payload.Sequence, Signature: tx.Signature})
	if err != nil {
		log.Fatalf("could not SendTx: %v", err)
	}
	log.Printf("transaction completed %v", r)
}

//create the instance of grpc client
func SendStackTxClient(stack Stack) {

	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewRpcClient(conn)

	// Contact the server and print out its response.
	name := defaultName
	log.Printf("transaction name %v", name)
	if len(os.Args) > 1 {
		name = os.Args[1]
	}
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	fmt.Println(ctx)
	defer cancel()
	var tx []*Transaction
	var sends []*pb.Send
	for i := 0; i < len(stack); i++ {
		st, _ := stack.Pop()
		err := json.Unmarshal(st, tx)
		if err != nil {
			fmt.Println("error:", err)
		}
		send := &pb.Send{
			To:     hex.EncodeToString(tx[i].Payload.Recipients.Address),
			Amount: tx[i].Payload.Recipients.Amount,
		}
		sends := append(sends, send)
		address := hex.EncodeToString(tx[i].Payload.Sender)
		r, err := c.SendTx(ctx, &pb.Transaction{Sender: address, Receiver: sends, PublicKey: tx[i].Public_key, Sequence: tx[i].Payload.Sequence, Signature: tx[i].Signature})
		if err != nil {
			log.Fatalf("could not SendTx: %v", err)
		}
		log.Printf("transaction completed %v", r)
	}

}
