package mangostine

import (
	"crypto/ed25519"

	"github.com/zeebo/blake3"
)

const (
	// AddressSize is the size of a pubkey address.
	AddressSize = 20
)

// An address is a []byte, but hex-encoded even in JSON.
// []byte leaves us the option to change the address length.
// Use an alias so Unmarshal methods (with ptr receivers) are available too.
type Address []byte

func FromPublic(pubkey ed25519.PublicKey) Address {
	hasher := blake3.New()
	pubhash := hasher.Sum(pubkey)
	addr := make([]byte, 20)
	copy(addr[:], pubhash[:20])
	return addr
}
