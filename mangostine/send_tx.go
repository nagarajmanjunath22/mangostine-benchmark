package mangostine

import (
	"crypto/ed25519"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"

	"github.com/ethereum/go-ethereum/rlp"
	"github.com/spf13/cobra"
)

type Payload struct {
	Sender     Address
	Recipients Recipient
	Sequence   uint64
}

type Recipient struct {
	Address Address
	Amount  uint64
}

type Transaction struct {
	Payload    Payload
	Public_key ed25519.PublicKey
	Signature  []byte
}

// stack to store all the transcation
type Stack [][]byte

// keysCmd represents the keys command
func SendTxCommand() *cobra.Command {

	sendCmd := &cobra.Command{
		Use:   "sendTx",
		Short: "send transcation network",
		Long:  `send transcation`,
		RunE: func(_ *cobra.Command, _ []string) error {
			kp := []Keypair{}
			file, _ := ioutil.ReadFile(DefaultHome + "key.json")
			err := json.Unmarshal([]byte(file), &kp)

			if err != nil {
				fmt.Println("reading json key file ", err)
			}
			numTx, err := createNode("Please enter number of transcation to be created")
			if err != nil {
				log.Fatal(err)
			}

			var transactions []Transaction

			sequence := uint64(0)
			amount := uint64(10)

			var pubkey ed25519.PublicKey
			var secretkey ed25519.PrivateKey
			for i := 0; i < len(kp); i++ {
				pubkey = kp[i].Publickkey
				secretkey = kp[i].Privatekey
			}
			// var stack Stack

			address := FromPublic(pubkey)
			for i := 0; i < numTx; i++ {

				var recipients []Recipient

				recipient := Recipient{
					Address: address,
					Amount:  amount,
				}
				recipients = append(recipients, recipient)

				var sig []byte
				//create the transcation
				println("the value in sequence", sequence)
				tx1 := &Transaction{
					Payload: Payload{
						Sequence:   sequence,
						Sender:     address,
						Recipients: recipient,
					},
					Public_key: pubkey,
					Signature:  sig,
				}

				message, _ := rlp.EncodeToBytes(tx1.Payload)
				// message, _ := json.Marshal(tx1.Payload)
				fmt.Println("marshal message ##########", message)
				fmt.Println("hex message ##########", hex.EncodeToString(message))

				signTx := ed25519.Sign(secretkey, message)

				fmt.Println("the sign signature ##########", signTx)
				fmt.Println("the signature hex ######", hex.EncodeToString(signTx))

				tx1.Signature = signTx

				if err != nil {
					fmt.Println("error:", err)
				}

				// var txjson, err = json.Marshal(tx1)
				// if err != nil {
				// 	log.Fatal(err)¸
				// }
				transactions = append(transactions, *tx1)
				sequence = sequence + 1
			}

			for _, v := range transactions {
				SendTXClient(&v)
			}

			return nil
		},
	}

	return sendCmd
}
