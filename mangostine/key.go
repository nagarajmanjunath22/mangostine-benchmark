package mangostine

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"

	"github.com/spf13/cobra"
	"github.com/tyler-smith/go-bip39"
	"golang.org/x/crypto/ed25519"
)

var (
	DefaultHome = os.ExpandEnv("$HOME/.mangostine/")
)

type Keypair struct {
	Mnemonic   string             `json:"mnemonic"`
	Privatekey ed25519.PrivateKey `json:"privatekey"`
	Publickkey ed25519.PublicKey  `json:"publickkey"`
	Address    Address            `json:"address"`
}

// keysCmd represents the keys command
func CreateKeyPairCommand() *cobra.Command {

	keysCmd := &cobra.Command{
		Use:   "keypair",
		Short: "create the account with mnemonic, private key, public key and address",
		Long:  `create the account with mnemonic, private key, public key and address`,
		RunE: func(_ *cobra.Command, _ []string) error {

			node, err := createNode("Please enter integer to create number of  keypair")
			kp := []Keypair{}
			for j := 0; j < node; j++ {
				if err != nil {
					fmt.Println("keypair creating", err)
				}
				_ = strconv.Itoa(j)
				entropy, _ := bip39.NewEntropy(256)
				mnemonic, _ := bip39.NewMnemonic(entropy)
				seed := bip39.NewSeed(mnemonic, "123456")
				first := seed[:32]
				privatekey := ed25519.NewKeyFromSeed(first)
				var publicKey ed25519.PublicKey
				publicKey = privatekey.Public().(ed25519.PublicKey)
				addr := FromPublic(publicKey)
				fmt.Println(addr)
				keys := Keypair{Mnemonic: mnemonic, Privatekey: privatekey, Publickkey: publicKey, Address: addr}
				kp = append(kp, keys)

			}

			kpjson, _ := json.Marshal(kp)
			outputDocumet, err := makeOutputFilepath(DefaultHome)
			err = ioutil.WriteFile(outputDocumet, kpjson, 0644)
			if err != nil {
				log.Fatal(err)
			}
			return nil
		},
	}

	return keysCmd
}
