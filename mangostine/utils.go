package mangostine

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
)

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}
func EnsuredeleteDir(dir string, mode os.FileMode) error {
	err := os.RemoveAll(dir)
	if err != nil {
		return fmt.Errorf("Could not create directory %v. %v", dir, err)
	}
	return nil
}
func EnsureDir(dir string, mode os.FileMode) error {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err := os.MkdirAll(dir, mode)
		if err != nil {
			return fmt.Errorf("Could not create directory %v. %v", dir, err)
		}
	}
	return nil
}

func makeOutputFilepath(rootDir string) (string, error) {
	writePath := filepath.Join(rootDir)
	if err := EnsureDir(writePath, 0700); err != nil {
		return "", err
	}
	return filepath.Join(writePath, fmt.Sprintf("key.json")), nil
}

//Take the input from the terminal for creating the node
func createNode(msg string) (int, error) {
	var n int
	fmt.Println(msg + ": ")
	return 10000, nil
	_, err := fmt.Scanf("%d", &n)
	if err != nil {
		fmt.Println(err)
	}
	if n > 20000 {
		fmt.Println("please enter below 15")
		os.Exit(1)
	}

	fmt.Println("You have entered : ", n)
	return n, nil
}

func WriteDataToFileAsJSON(data interface{}, filedir string) (int, error) {
	//write data as buffer to json encoder
	buffer := new(bytes.Buffer)
	encoder := json.NewEncoder(buffer)
	encoder.SetIndent("", "\t")

	err := encoder.Encode(data)
	if err != nil {
		return 0, err
	}
	file, err := os.OpenFile(filedir, os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		return 0, err
	}
	n, err := file.Write(buffer.Bytes())
	if err != nil {
		return 0, err
	}
	return n, nil
}

// IsEmpty: check if stack is empty
func (s *Stack) IsEmpty() bool {
	return len(*s) == 0
}

// Push a new value onto the stack
func (s *Stack) Push(tx []byte) {
	*s = append(*s, tx) // Simply append the new value to the end of the stack
}

// Remove and return top element of stack. Return false if stack is empty.
func (s *Stack) Pop() ([]byte, bool) {
	if s.IsEmpty() {
		return nil, false
	} else {
		index := len(*s) - 1   // Get the index of the top most element.
		element := (*s)[index] // Index into the slice and obtain the element.
		*s = (*s)[:index]      // Remove it from the stack by slicing it off.
		return element, true
	}
}
