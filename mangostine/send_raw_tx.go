package mangostine

import (
	"crypto/ed25519"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"

	"github.com/ethereum/go-ethereum/rlp"
	"github.com/spf13/cobra"
)

// keysCmd represents the keys command
func Send_Raw_TxCommand() *cobra.Command {

	sendCmd := &cobra.Command{
		Use:   "send_rawTx",
		Short: "send transcation network",
		Long:  `send transcation`,
		RunE: func(_ *cobra.Command, _ []string) error {
			kp := []Keypair{}
			file, _ := ioutil.ReadFile(DefaultHome + "key.json")
			err := json.Unmarshal([]byte(file), &kp)

			if err != nil {
				fmt.Println("reading json key file ", err)
			}
			numTx, err := createNode("Please enter number of transcation to be created")
			if err != nil {
				log.Fatal(err)
			}

			// var stack Stack
			j := uint64(1)
			amount := uint64(10)
			sequence := j
			var pubkey ed25519.PublicKey
			var secretkey ed25519.PrivateKey
			for i := 0; i < len(kp); i++ {
				pubkey = kp[i].Publickkey
				secretkey = kp[i].Privatekey
			}
			// var stack Stack

			address := FromPublic(pubkey)
			for i := 0; i < numTx; i++ {

				var recipients []Recipient

				recipient := Recipient{
					Address: address,
					Amount:  amount,
				}
				recipients = append(recipients, recipient)

				var sig []byte
				//create the transcation
				println("the value in sequence", sequence)
				tx1 := &Transaction{
					Payload: Payload{
						Sequence:   sequence,
						Sender:     address,
						Recipients: recipient,
					},
					Public_key: pubkey,
					Signature:  sig,
				}

				message, _ := rlp.EncodeToBytes(tx1.Payload)

				fmt.Println("marshal message ##########", message)
				fmt.Println("hex message ##########", hex.EncodeToString(message))

				signTx := ed25519.Sign(secretkey, message)

				fmt.Println("the sign signature ##########", signTx)
				fmt.Println("the signature hex ######", hex.EncodeToString(signTx))

				tx1.Signature = signTx

				txrlp, err := rlp.EncodeToBytes(tx1)
				if err != nil {
					fmt.Println("error:", err)
				}
				SendRawTxClient(txrlp)
				// stack.Push(tx1)
				sequence = j + 1
			}
			return nil
		},
	}

	return sendCmd
}
